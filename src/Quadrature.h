
#include <math.h>
#include "SELF_Precision.h"

#ifndef pi
#define pi 3.1415926535897932384626433832795
#endif

#ifndef TOL
#define TOL 4.44*pow(10.0,-16)
#endif

void Legendre_Polynomial( real *L, real *dLds, real s, int N );
void Legendre_Gauss( real *nodes, real *weights, int N );
void Legendre_Gauss_Lobatto( real *nodes, real *weights, int N );
