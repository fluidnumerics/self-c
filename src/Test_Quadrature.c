

#include <stdio.h>
#include <stdlib.h>
#include "Quadrature.h"
#include "SELF_Precision.h"


void Test_Legendre_Polynomial()
{

  real L, dLds;
  real s;
  int    i, j;
  int    N=15;
  int    M=100;
  char   file_name[32];
  FILE   *fp;



  for ( j=0; j<=N; j++ ){

    snprintf( file_name, sizeof(char)*32, "Legendre.%03d.curve", j );
    fp = fopen(file_name,"w");
    fprintf( fp, "#LegendrePoly\n" );

    for ( i=0; i<=M; i++ ){

      s = -1.0 + (2.0/M)*i;
      Legendre_Polynomial( &L, &dLds, s, j );
      fprintf( fp, " %f %f\n", s, L );
      
    }
    
    fclose( fp );

  }

}

void Test_Legendre_Gauss()
{
/*
 For reference, nodes and weights can be compared with the weights and abscissae tables
 at https://pomax.github.io/bezierinfo/legendre-gauss.html

*/
  real *nodes, *weights;
  int    N=15;
  int    i, j;
  char   file_name[32];
  FILE   *fp;

  for ( j=0; j<=N; j++ ){

    snprintf( file_name, sizeof(char)*32, "Legendre_Gauss.%03d.txt", j );
    fp = fopen(file_name,"w");
    fprintf( fp, "nodes, weights\n" );

    nodes   = (real*)malloc( (j+1)*sizeof(real) );
    weights = (real*)malloc( (j+1)*sizeof(real) );

    Legendre_Gauss( nodes, weights, j );

    for ( i=0; i<=j; i++ ){

      fprintf( fp, " %f %f\n", nodes[i], weights[i] );

    }

    free( nodes );
    free( weights );

  }
}

void Test_Legendre_Gauss_Lobatto()
{
/*
 For reference, nodes and weights can be compared with the weights and abscissae tables
 at http://mathworld.wolfram.com/LobattoQuadrature.html

*/
  real *nodes, *weights;
  int    N=15;
  int    i, j;
  char   file_name[32];
  FILE   *fp;

  for ( j=0; j<=N; j++ ){

    snprintf( file_name, sizeof(char)*32, "Legendre_Gauss_Lobatto.%03d.txt", j );
    fp = fopen(file_name,"w");
    fprintf( fp, "nodes, weights\n" );

    nodes   = (real*)malloc( (j+1)*sizeof(real) );
    weights = (real*)malloc( (j+1)*sizeof(real) );

    Legendre_Gauss_Lobatto( nodes, weights, j );

    for ( i=0; i<=j; i++ ){

      fprintf( fp, " %f %f\n", nodes[i], weights[i] );

    }

    free( nodes );
    free( weights );

  }
}
void main()
{

 Test_Legendre_Polynomial();

 Test_Legendre_Gauss();

 Test_Legendre_Gauss_Lobatto();

}
