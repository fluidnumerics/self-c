
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include "Lagrange.h"
#include "SELF_Precision.h"


// Manual Constructor : Allocates space for the interpolant
void Lagrange_Init( struct Lagrange *interp, int N )
{

  interp->interp_nodes = (real*)malloc( (N+1)*sizeof(real) ); 
  interp->barycentric_weights = (real*)malloc( (N+1)*sizeof(real) ); 
  interp->derivative_matrix   = (real*)malloc( (N+1)*(N+1)*sizeof(real) );

  interp->polynomial_degree = N;

  // Initialize memory to 0.0
  for( int j=0; j <= N; j++ )
  {
    interp->interp_nodes[j]        = 0.0;
    interp->barycentric_weights[j] = 0.0;

    for( int i=0; i <= N; i++ )
    {
      interp->derivative_matrix[INDEX(i,j,N)] = 0.0;
    }
  }

}

// Manual Destructor : Frees space held by the interpolant
void Lagrange_Free( struct Lagrange *interp )
{

  free( interp->derivative_matrix );
  free( interp->barycentric_weights );
  free( interp->interp_nodes );

}

