
#include <math.h>
#include <stdlib.h>
#include "SELF_Precision.h"

#include "Quadrature.h"


void Legendre_Polynomial( real *L, real *dLds, real s, int N )
{
/*
  From Kopriva (2009) "Implementing Spectral Methods for Partial Differential Equations", p. 63, Algorithm 22

  Evaluates the Nth Legendre polynomials and it's derivative at the  point (s), which is between [-1,1]
*/

  real lnM2, lnM1, dLnM2, dLnM1;
  int    i;

  if( N == 0 )
  {
    *L    = 1.0;
    *dLds = 0.0;
  }
  else if( N == 1 )
  {
    *L    = s;
    *dLds = 1.0;
  }
  else
  {

    lnM2  = 1.0;
    lnM1  = s;
    dLnM2 = 0.0;
    dLnM1 = 1.0;

    for( i=2; i <= N; i++ )
    {
 
      *L = ( (2.0*i - 1.0 )*s*lnM1 - (i - 1.0)*lnM2)/i;
      *dLds = dLnM2 + (2.0*i-1.0)*lnM1;
      
      lnM2 = lnM1;
      lnM1 = *L;
      dLnM2 = dLnM1;
      dLnM1 = *dLds;
  
    }
  }
}

void LegendreQandL( real *q, real *qprime, real *L, real s, int N )
{
/*
  From Kopriva (2009) "Implementing Spectral Methods for Partial Differential Equations", p. 65, Algorithm 24
*/
  int i;
  real lnM2, lnM1, dLnM2, dLnM1, dLn1, ln1;
  real dLds;

  lnM2  = 1.0;
  lnM1  = s;
  dLnM2 = 0.0;
  dLnM1 = 1.0;

  for( i=2; i<=N; i++ )
  {
   
      *L = ( (2.0*i - 1.0 )*s*lnM1 - (i - 1.0)*lnM2)/i;
      dLds = dLnM2 + (2.0*i-1.0)*lnM1;

      lnM2 = lnM1;
      lnM1 = *L;

      dLnM2 = dLnM1;
      dLnM1 = dLds;

  }

  i = N+1;
  ln1  = ( (2.0*i - 1.0 )*s*(*L) - (i - 1.0)*lnM2)/i;
  dLn1 = dLnM2 + (2.0*i-1.0)*lnM1;
  *q = ln1 - lnM2;
  *qprime = dLn1 - dLnM2;

}

void Legendre_Gauss( real *nodes, real *weights, int N )
{
/*
  From Kopriva (2009) "Implementing Spectral Methods for Partial Differential Equations", p. 64, Algorithm 23
*/
  real Ln1, dLn1, delta;
  int    j, k;


  if( N == 0 )
  {

    nodes[0]   = 0.0;
    weights[0] = 2.0;

  } 
  else if( N == 1 )
  {

    nodes[0]   = -sqrt( 1.0/3.0 );
    nodes[1]   = sqrt( 1.0/3.0 );
    weights[0] = 1.0;
    weights[1] = 1.0;

  }
  else
  {

    for( j=0; j<((N+1)/2); j++  )
    {
    
      nodes[j] = -cos( ( 2.0*j + 1.0)*pi/(2.0*N + 2.0) );
      for( k = 1; k < 100; k++ )
      {

        Legendre_Polynomial( &Ln1, &dLn1, nodes[j], N+1 );

        delta = -Ln1/dLn1;
        nodes[j] += delta;

        if( abs( delta ) <= TOL*nodes[j] )
        {

          break;

        }

      }

      Legendre_Polynomial( &Ln1, &dLn1, nodes[j], N+1 );
      weights[j]   = 2.0/( (1.0-nodes[j]*nodes[j])*dLn1*dLn1 );
      weights[N-j] = 2.0/( (1.0-nodes[j]*nodes[j])*dLn1*dLn1 );
      nodes[N-j]   = -nodes[j];
      
    }

  }

  if( N % 2 == 0 && N != 0 )
  {

    Legendre_Polynomial( &Ln1, &dLn1, 0.0, N+1 );
    weights[N/2] = 2.0/( dLn1*dLn1 );
    nodes[N/2]   = 0.0;

  }

}

void Legendre_Gauss_Lobatto( real *nodes, real *weights, int N )
{
/*
  From Kopriva (2009) "Implementing Spectral Methods for Partial Differential Equations", p. 66, Algorithm 25
*/
  real q, qprime, L, delta;
  int    j, k;


  if( N == 0 )
  {

    nodes[0]   = 0.0;
    weights[0] = 2.0;

  }
  else if( N == 1 )
  {

    nodes[0]   = -1.0;
    weights[0] = 1.0;

    nodes[1]   = 1.0;
    weights[1] = 1.0;
    

  }
  else
  {

    nodes[0]   = -1.0;
    weights[0] = 2.0/(N*(N+1));

    nodes[N]   = 1.0;
    weights[N] = 2.0/(N*(N+1));
 
    for( j=1; j<=((N+1)/2-1); j++ )
    {

      nodes[j] = -cos( (j+0.25)*pi/N - 3.0/(8.0*N*pi*(j+0.25)) );
      for( k = 1; k < 500; k++ )
      {

        LegendreQandL( &q, &qprime, &L, nodes[j], N );
        delta     = -q/qprime;
        nodes[j] += delta;
        
        if( abs( delta ) <= TOL*nodes[j] )
        {

          break;

        }

      }

      LegendreQandL( &q, &qprime, &L, nodes[j], N );
      weights[j] = 2.0/(N*(N+1)*L*L);

      weights[N-j] = weights[j];
      nodes[N-j]   = -nodes[j];

    }
    

  }

  if( N % 2 == 0 && N != 0 )
  {

    LegendreQandL( &q, &qprime, &L, 0.0, N );
    weights[N/2] = 2.0/( N*(N+1)*L*L );
    nodes[N/2]   = 0.0;

  }
 
  


}
