#ifndef LAGRANGE_H
#define LAGRANGE_H


#include "SELF_Precision.h"

#define INDEX(i,j,N) i+j*(N+1)

typedef struct Lagrange{
  int polynomial_degree;
  real *interp_nodes;
  real *barycentric_weights;
  real *derivative_matrix;

#ifdef HAVE_CUDA
  real *interp_nodes_dev;
  real *barycentric_weights_dev;
  real *derivative_matrix_dev;
#endif

}Lagrange;



// Manual Constructor : Allocates space for the interpolant
void Lagrange_Init( struct Lagrange *interp, int N );

// Manual Destructor : Frees space held by the interpolant
void Lagrange_Free( struct Lagrange *interp );


#endif
